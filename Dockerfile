FROM python:3.5.2

RUN mkdir -p /opt/src/
WORKDIR /opt/src/

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY ./src /opt/src

RUN mkdir /data

CMD ["python", "main.py"]
