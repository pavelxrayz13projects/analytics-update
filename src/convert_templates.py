import json

with open('../data/templates_old.json') as old_templates_file:
    old_templates = json.load(old_templates_file)


new_templates = []
for template in old_templates:
    new_template = {
        '_id': template.get('template_name'),
        'template': {}
    }
    for key, value in template.items():
        if key == 'template_name':
            continue
        new_template['template'][key] = value
    new_templates.append(new_template)

with open('../data/templates.json', 'w') as new_templates_file:
    json.dump(new_templates, new_templates_file, ensure_ascii=False)
