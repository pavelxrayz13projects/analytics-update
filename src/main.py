"""
Обновление базы данных
"""
import json
import os

from mongoengine import connect

from models import (Rule, TicketTemplate, Tag, Metarule)


UPDATE_PATH = '/data/'


def create_objects(model):
    file_path = os.path.join(UPDATE_PATH, model.update_filename)
    with open(file_path) as data_file:
        items = json.load(data_file)

    for item in items:
        model(**item).save()


def main():
    host = os.getenv('DB_HOST', '127.0.0.1')
    port = int(os.getenv('DB_PORT', '27017'))
    name = os.getenv('DB_NAME', 'analytics')

    connect(db=name, host=host, port=port)

    models_to_update = [Rule, TicketTemplate, Tag, Metarule]
    for model in models_to_update:
        create_objects(model)


if __name__ == '__main__':
    main()
