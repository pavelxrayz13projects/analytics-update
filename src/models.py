"""
Модели для инициализационных данных
"""
import mongoengine


class Rule(mongoengine.Document):
    """
    Исходный текст правила
    """
    _id = mongoengine.StringField(verbose_name='Идентификатор правила')

    text = mongoengine.StringField(verbose_name='Исходный текст правила')

    update_filename = 'rules.json'

    meta = {
        'collection': 'rules',
        'strict': False
    }


class TicketTemplate(mongoengine.Document):
    """
    Шаблон инцидента
    """
    _id = mongoengine.StringField(verbose_name='Наименование')

    template = mongoengine.DictField(verbose_name='Шаблон')

    update_filename = 'templates.json'

    meta = {
        'collection': 'ticket_templates',
        'strict': False,
    }


class Tag(mongoengine.Document):
    """
    Тэги правил
    """
    _id = mongoengine.StringField(verbose_name='Тэги как один документ')

    tags = mongoengine.DictField(verbose_name='Тэги')

    update_filename = 'tags.json'

    meta = {
        'collection': 'tags',
        'strict': False
    }


class Metarule(mongoengine.Document):
    """
    Класс метаправил
    """
    name = mongoengine.StringField(primary_key=True)

    template = mongoengine.StringField()

    algorithm = mongoengine.StringField()

    parameters = mongoengine.DictField()

    update_filename = 'metarules.json'

    meta = {
        'collection': 'metarules',
        'strict': False
    }
